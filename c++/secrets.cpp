#include <string>
#include <vector>

using namespace std;

namespace jh {
    vector<string> secrets {
        "australian"s,
            "research"s,
            "firm"s,
            "morningstar"s,
            "issuing"s,
            "some"s,
            "reassuring"s,
            "words"s,
            "about"s,
            "future"s,
            "important"s,
            "tiwai"s,
            "point"s,
            "aluminium"s,
            "smelter,"s,
            "believing"s,
            "will"s,
            "close"s,
            "medium"s,
            "term"s,
            "analyst"s,
            "adrian"s,
            "atkins"s,
            "said"s,
            "main"s,
            "risks"s,
            "facing"s,
            "zealand"s,
            "electricity"s,
            "industry"s,
            "potential"s,
            "closure"s,
            "smelter"s,
            "smelter"s,
            "used"s,
            "zealand"s,
            "electricity"s,
            "closure"s,
            "would"s,
            "leave"s,
            "industry"s,
            "significant"s,
            "oversupply"s,
            "contact"s,
            "energy"s,
            "meridian"s,
            "energy"s,
            "would"s,
            "most"s,
            "exposed"s,
            "given"s,
            "proximity"s,
            "their"s,
            "south"s,
            "island"s,
            "hydro-electric"s,
            "schemes"s,
            "smelter"s,
            "morningstar"s,
            "previously"s,
            "noted"s,
            "improving"s,
            "aluminium"s,
            "price"s,
            "reduced"s,
            "risk"s,
            "smelter"s,
            "closing"s,
            "near"s,
            "term."s,
            "since"s,
            "then,"s,
            "aluminium"s,
            "price"s,
            "risen"s,
            "further"s,
            "sitting"s,
            "roughly"s,
            "pound"s,
            "price"s,
            "aluminium"s,
            "highest"s,
            "been"s,
            "since"s,
            "said"s,
            "although"s,
            "morningstar"s,
            "remained"s,
            "bearish"s,
            "aluminium"s,
            "longer"s,
            "term,"s,
            "stockpiles"s,
            "warehouses"s,
            "monitored"s,
            "london"s,
            "metals"s,
            "exchange"s,
            "shrunk"s,
            "levels,"s,
            "which"s,
            "supportive"s,
            "least"s,
            "near"s,
            "term"s
    };
}
