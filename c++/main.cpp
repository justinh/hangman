#include "hangman.h"
#include <cstdlib>

using namespace std;
using namespace jh;

// this is in a separate file so that we can build either to test or to run app
int main(int argc, char *argv[])
{
    srand(time(NULL));
    auto secret = secrets[rand() % secrets.size()];

    auto gameResult = MakeAGuess(MaxNumGuesses, secret, {}, GetGuess, EvaluateGuess);
    switch (gameResult) {
        case Game::HasQuit: return 1;
        case Game::HasWon: cout << "the word was \"" << secret << "\". you have won!!\n"; return 1;
        default: cout << "the word was \"" << secret << "\". you lost :(\n"; return 2;
    }
}


