#include <experimental/optional>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

namespace jh {

    const int MaxNumGuesses = 7;
    const char MaskChar = '.'; // used to hide characgers not guessed
    enum class Guess { Valid, Invalid, AlreadyTested };
    enum class Game { HasWon, HasLost, HasQuit };

    extern std::vector<std::string> secrets; // a list of words to be used as the secret

    // most of these methods are only here in the header to expose for testing
    Game MakeAGuess(int numInvalidGuessesLeft,
            std::string secret,
            std::string previousGuesses,
            const std::function<std::experimental::optional<char>(std::string, int)> &getGuess,
            const std::function<Guess (char, std::string, std::string)> &evaluateGuess);

    std::experimental::optional<char> GetGuess(const std::string &maskedSecret, int numGuessesRemaining);
    Guess EvaluateGuess(char toEvaluate, std::string secret, std::string previousGuesses);
    bool HasWon(std::string secret);
    std::string MaskIfNotFound(std::string secret, std::string toFind);

    // some very generic methods
    template <typename List> List RemoveFromCollection(List list, List toRemove)
    {
        for (auto element : toRemove) {
            list.erase(remove(begin(list), end(list), element), end(list));
        }
        return list;
    }

    template <typename List, typename Element> List AddToCollection(List list, Element toAdd)
    {
        list.push_back(toAdd);
        return list;
    }
}

