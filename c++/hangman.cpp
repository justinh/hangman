#include <algorithm>
#include <iostream>
#include <vector>
#include "hangman.h"

using namespace std;
using namespace std::experimental;
using namespace jh;


namespace jh {
    Game MakeAGuess(int numInvalidGuessesLeft,
            string secret,
            string previousGuesses,
            const function<optional<char>(string, int)> &getGuess,
            const function<Guess (char, string, string)> &evaluateGuess)
    {
        // this is the logic of the game
        // recursive (we can't really have a lot of guesses in such a game) so only called once externally
        // methods getGuess and evaluateGuess passed in to make it possible to unit test this method

        if (numInvalidGuessesLeft == 0) {
            return Game::HasLost;
        }

        auto guess = getGuess(MaskIfNotFound(secret, previousGuesses), numInvalidGuessesLeft);
        if (!guess) {
            return Game::HasQuit;
        }

        auto guessType = evaluateGuess(static_cast<char>(*guess), secret, previousGuesses);
        switch (guessType) {
            case Guess::AlreadyTested:
                return MakeAGuess(numInvalidGuessesLeft, secret, previousGuesses, getGuess, evaluateGuess);

            case Guess::Valid:
               {
                   auto stillToFind = RemoveFromCollection(secret, AddToCollection(previousGuesses, *guess));
                   if (stillToFind.empty()) {
                       return Game::HasWon;
                   }

                   return MakeAGuess(numInvalidGuessesLeft, secret, AddToCollection(previousGuesses, *guess), getGuess, evaluateGuess);
               }

            default:
               return MakeAGuess(numInvalidGuessesLeft - 1, secret, AddToCollection(previousGuesses, *guess), getGuess, evaluateGuess);
        }
    }

    optional<char> GetGuess(const string &maskedSecret, int numGuessesRemaining)
    {
        // get a guess from the user - show user where they are so far
        // convert to lowercase to simplify tests
        cout << "solve this string <" << maskedSecret << "> " << numGuessesRemaining << " guesses remaining - enter a guess followed by <enter> (or just <enter> to quit) ";
        string input;
        getline(cin, input);
        auto guess = input[0];

        return 0 == guess ? optional<char>() : tolower(guess);
    }

    Guess EvaluateGuess(char toEvaluate, string secret, string previousGuesses)
    {
        if (previousGuesses.find(toEvaluate) != string::npos) {
            return Guess::AlreadyTested;
        }

        if (secret.find(toEvaluate) != string::npos) {
            return Guess::Valid;
        }

        return Guess::Invalid;
    }

    string MaskIfNotFound(string secret, string toFind)
    {
        // create a string that hides the characters that have not been guessed
        string masked;
        transform(cbegin(secret), cend(secret), back_inserter(masked), [&toFind] (const auto &elem) {
            return toFind.find(elem) == string::npos ? MaskChar : elem;
        });

        return masked;
    }
}

