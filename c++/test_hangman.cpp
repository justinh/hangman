#include "catch.h"
#include "hangman.h"

using namespace std;
using namespace jh;

SCENARIO("guesses can be evaluated correctly", "[jh]") {
    GIVEN("a secret to search") {
        WHEN("the guess is valid") {
            THEN("evaluate should return valid") {
                REQUIRE(EvaluateGuess('t', "string"s, ""s) == Guess::Valid);
            }
        }
        WHEN("the guess is not valid") {
            THEN("evaluate should return invalid") {
                REQUIRE(EvaluateGuess('c', "string"s, ""s) == Guess::Invalid);
            }
        }
        WHEN("the guess has already been tried") {
            THEN("evaluate should say so") {
                REQUIRE(EvaluateGuess('t', "string"s, "tr"s) == Guess::AlreadyTested);
            }
        }
    }
}

SCENARIO("elements can be removed from collection", "[jh]") {
    GIVEN("a valid collection") {
        auto collection = "something there"s;
        auto toRemove = "te"s;

        WHEN("entry is removed from collection") {
            auto newCollection = RemoveFromCollection(collection, toRemove);
            THEN("collection does not contain removed element") {
                REQUIRE(newCollection == "somhing hr");
            }
        }
    }
}

SCENARIO("elements can be added to collection", "[jh]") {
    GIVEN("a valid collection") {
        auto collection = "something"s;
        auto toAdd = 'z';

        REQUIRE(collection.find(toAdd) == string::npos);
        WHEN("entry is added to collection") {
            auto newCollection = AddToCollection(collection, toAdd);
            THEN("collection contains added element") {
                REQUIRE(newCollection == "somethingz");
            }
        }
    }
}

SCENARIO("can play game according to rules") {
    GIVEN("a secret and a game") {
        auto secret = "this is a secret";

        WHEN("too many bad guesses are made") {
            auto result = MakeAGuess(MaxNumGuesses, secret, ""s, [] (const auto &, const auto) { return 'z'; }, [] (const auto &, const auto &, const auto &) { return Guess::Invalid; });
            THEN("we lose") {
                REQUIRE(result == Game::HasLost);
            }
        }
        WHEN("lots of good guesses are made") {
            auto guessCount = 0;
            auto guessFn = [secret, &guessCount] (const auto &, const auto) { return secret[guessCount++]; };
            auto result = MakeAGuess(MaxNumGuesses, secret, ""s, guessFn, [] (const auto &, const auto &, const auto &) { return Guess::Valid; });
            THEN("we win") {
                REQUIRE(result == Game::HasWon);
            }
        }
        WHEN("six bad guesses are made") {
            auto guessCount = 0;
            auto guessList = "bdfgjk acehirst"s;
            auto guessFn = [guessList, &guessCount] (const auto &, const auto) { return guessList[guessCount++]; };
            auto result = MakeAGuess(MaxNumGuesses, secret, ""s, guessFn, EvaluateGuess);
            THEN("we win") {
                REQUIRE(result == Game::HasWon);
            }
        }
        WHEN("seven bad guesses are made") {
            auto guessCount = 0;
            auto guessList = "bdfgjkl acehirst"s;
            auto guessFn = [guessList, &guessCount] (const auto &, const auto) { return guessList[guessCount++]; };
            auto result = MakeAGuess(MaxNumGuesses, secret, ""s, guessFn, EvaluateGuess);
            THEN("we lose") {
                REQUIRE(result == Game::HasLost); }
        }
    }
}

SCENARIO("can mask out characters") {
    GIVEN("a string and a filter") {
        auto toMask = "this is a string"s;
        auto filter = "is"s;
        WHEN("string is masked") {
            auto masked = MaskIfNotFound(toMask, filter);
            THEN("masked string is as expected") {
                REQUIRE(masked == "..is.is...s..i..");
            }
        }
    }
}



