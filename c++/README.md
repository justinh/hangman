# hangman

### to build unit tests

```
clang++ -std=c++17 hangman.cpp unittest.cpp test_hangman.cpp
```
or
```
g++ -std=c++17 hangman.cpp unittest.cpp test_hangman.cpp
```

### and run unit tests

```
./a.out
```

### to build the application

```
clang++ -std=c++17 hangman.cpp main.cpp secrets.cpp
```
or
```
g++ -std=c++17 hangman.cpp main.cpp secrets.cpp
```

### and run the application

```
./a.out
```

## files

| File | Description |
| ---- | ----------- |
| main.cpp | starting point of application |
| hangman.cpp | application logic |
| hangman.h | header file used by app and unit tests |
| secrets.cpp | a list of words to prime application |
| unittest.cpp | empty file for unit test config |
| test_hangman.cpp | unit tests |

