# hangman

### to build and run

open project in visual studio 2017

use test explorer to run unit tests, and run/build the application as normal - it is a command line application

## files

| File | Description |
| ---- | ----------- |
| Program.cs | starting point of application |
| hangman.cs | application logic |
| secrets.cpp | a list of words to prime application |
| test_hangman.cpp | unit tests |
