using System;
using System.Linq;

namespace hangman
{
    static public class Hangman
    {
        const char MaskChar = '.'; // used to hide characters not guessed
        public enum Guess { Valid, Invalid, AlreadyTested };
        public enum Game { HasWon, HasLost, HasQuit };

        static public Game MakeAGuess(int numInvalidGuessesLeft,
            string secret,
            string previousGuesses,
            Func<string, int, char?> getGuess,
            Func<char, string, string, Guess> evaluateGuess)
        {
            // this is the logic of the game
            // recursive (we can't really have a lot of guesses in such a game) so only called once externally
            // methods getGuess and evaluateGuess passed in to make it possible to unit test this method

            if (numInvalidGuessesLeft == 0) {
                return Game.HasLost;
            }

            var guess = getGuess(MaskIfNotFound(secret, previousGuesses), numInvalidGuessesLeft);
            if (!guess.HasValue) {
                return Game.HasQuit;
            }

            var guessType = evaluateGuess(guess.Value, secret, previousGuesses);
            switch (guessType) {
                case Guess.AlreadyTested:
                    return MakeAGuess(numInvalidGuessesLeft, secret, previousGuesses, getGuess, evaluateGuess);

                case Guess.Valid: {
                        var stillToFind = new String(secret.Except(previousGuesses + guess.Value).ToArray());
                        if (stillToFind.Length == 0) {
                            return Game.HasWon;
                        }

                        return MakeAGuess(numInvalidGuessesLeft, secret, previousGuesses + guess.Value, getGuess, evaluateGuess);
                    }

                default:
                    return MakeAGuess(numInvalidGuessesLeft - 1, secret, previousGuesses + guess.Value, getGuess, evaluateGuess);
            }
        }

        static public char? GetGuess(string maskedSecret, int numGuessesRemaining)
        {
            // get a guess from the user - show user where they are so far
            // convert to lowercase to simplify tests
            Console.Write($"solve this string <{maskedSecret}> {numGuessesRemaining} guesses remaining - enter a guess followed by <enter> (or just <enter> to quit) ");
            var input = Console.ReadLine();
            return input.Count() == 0 ? (char?)null : Char.ToLower(input[0]);
        }

        static public Guess EvaluateGuess(char toEvaluate, string secret, string previousGuesses)
        {
            if (previousGuesses.IndexOf(toEvaluate) != -1) {
                return Guess.AlreadyTested;
            }

            if (secret.IndexOf(toEvaluate) != -1) {
                return Guess.Valid;
            }

            return Guess.Invalid;
        }

        static public string MaskIfNotFound(string secret, string toFind)
        {
            // create a string that hides the characters that have not been guessed
            var masked = new String(secret.Select(x => toFind.Contains(x) ? x : MaskChar).ToArray());
            return masked;
        }
    }
}

